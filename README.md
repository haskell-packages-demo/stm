# stm

Software Transactional Memory. https://www.stackage.org/package/stm

# Official documentation
* [*Software transactional memory*
  ](https://wiki.haskell.org/Software_transactional_memory)
* [Simple STM example
  ](https://wiki.haskell.org/Simple_STM_example)

# Unofficial documentation
* [*Beautiful concurrency*
  ](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/beautiful.pdf)
  2007-05
  Simon Peyton Jones,
  Microsoft Research, Cambridge
  * to appear in “Beautiful code”, ed Greg Wilson, O’Reilly 2007